package com.pashiur.moviestore.service.impl;

import com.pashiur.moviestore.dao.UserDao;
import com.pashiur.moviestore.entity.User;
import com.pashiur.moviestore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public User getById(int id) {
        return userDao.getById(id);
    }
}
