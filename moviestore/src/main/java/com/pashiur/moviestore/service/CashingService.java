package com.pashiur.moviestore.service;

import com.pashiur.moviestore.entity.Country;
import com.pashiur.moviestore.entity.Genre;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class CashingService {
    private final Logger log = LoggerFactory.getLogger(getClass());

    private static final int CACHE_UPDATE_DELAY = 1000 * 60 * 60 * 4; // 4 hours

    @Autowired
    CountryService countryService;

    @Autowired
    GenreService genreService;

    private Map<Integer, Genre> genreMap = new ConcurrentHashMap<>();
    private Map<Integer, Country> countryMap = new ConcurrentHashMap<>();

    public Map<Integer, Genre> getGenreMap() {
        return genreMap;
    }

    public Map<Integer, Country> getCountryMap() {
        return countryMap;
    }

    @Scheduled(fixedDelay = CACHE_UPDATE_DELAY)
    public void cacheUpdate() {
        log.info("Start cache updating");
        long startTime = System.currentTimeMillis();
        List<Genre> genres = genreService.getAll();
        List<Country> countries = countryService.getAll();
        for (Genre genre : genres) {
            this.genreMap.put(genre.getId(), genre);
        }
        for (Country country : countries) {
            this.countryMap.put(country.getId(), country);
        }
        log.info("End cache updating", System.currentTimeMillis() - startTime);
    }
}
