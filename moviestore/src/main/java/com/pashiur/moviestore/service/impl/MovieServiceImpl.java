package com.pashiur.moviestore.service.impl;

import com.pashiur.moviestore.dao.MovieDao;
import com.pashiur.moviestore.entity.Country;
import com.pashiur.moviestore.entity.Genre;
import com.pashiur.moviestore.entity.Movie;
import com.pashiur.moviestore.filters.MovieFilter;
import com.pashiur.moviestore.service.*;
import com.pashiur.moviestore.util.MovieComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MovieServiceImpl implements MovieService {
    @Autowired
    private MovieDao movieDao;

    @Autowired
    private CountryService countryService;

    @Autowired
    private GenreService genreService;

    @Autowired
    private ReviewService reviewService;

    @Autowired
    private CashingService cashingService;

    private void setCountryGenreReviews(Movie movie) {
        movie.setCountry(getCountriesByMovieIdFromCash(movie.getId()));
        movie.setGenre(getGenresByMovieIdFromCash(movie.getId()));
        movie.setReviews(reviewService.getReviewsByMovie(movie));
    }

    @Override
    public List<Movie> getAll(String ratingOrder, String priceOrder) {
        List<Movie> movies = movieDao.getAll();
        for (Movie movie : movies) {
            setCountryGenreReviews(movie);
        }
        if (ratingOrder != null || priceOrder != null) {
            movies.sort(new MovieComparator(ratingOrder, priceOrder));
        }
        return movies;
    }

    @Override
    public Movie getById(int id) {
        Movie movie = movieDao.getById(id);
        setCountryGenreReviews(movie);
        return movie;
    }

    @Override
    public List<Movie> search(MovieFilter movieFilter) {
        List<Movie> movies = movieDao.search(movieFilter);
        for (Movie movie : movies) {
            setCountryGenreReviews(movie);
        }
        return movies;
    }

    private List<Genre> getGenresByMovieIdFromCash(Integer id) {

        return genreService.getGenresByMovieId(id).stream().map(Genre -> cashingService.getGenreMap().get(Genre.getId())).collect(Collectors.toList());
    }

    private List<Country> getCountriesByMovieIdFromCash(Integer id) {
        return countryService.getCountriesByMovieId(id).stream().map(Country -> cashingService.getCountryMap().get(Country.getId())).collect(Collectors.toList());
    }

}
