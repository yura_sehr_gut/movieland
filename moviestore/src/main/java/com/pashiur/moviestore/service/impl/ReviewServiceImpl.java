package com.pashiur.moviestore.service.impl;

import com.pashiur.moviestore.dao.*;
import com.pashiur.moviestore.entity.Movie;
import com.pashiur.moviestore.entity.Review;
import com.pashiur.moviestore.service.ReviewService;
import com.pashiur.moviestore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReviewServiceImpl implements ReviewService {

    @Autowired
    private UserService userService;

    @Autowired
    private ReviewDao reviewDao;

    @Override
    public List<Review> getReviewsByMovie(Movie movie) {

        List<Review> reviews = reviewDao.getReviewsByMovieId(movie.getId());
        for (Review review : reviews) {
            review.setUser(userService.getById(review.getUser().getId()));
            review.setMovie(movie);
        }
        return reviews;
    }
}
