package com.pashiur.moviestore.service;

import com.pashiur.moviestore.entity.Movie;
import com.pashiur.moviestore.entity.Review;

import java.util.List;

public interface ReviewService {
    List<Review> getReviewsByMovie(Movie movie);
}
