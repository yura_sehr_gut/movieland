package com.pashiur.moviestore.service.impl;

import com.pashiur.moviestore.dao.CountryDao;
import com.pashiur.moviestore.entity.Country;
import com.pashiur.moviestore.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CountryServiceImpl implements CountryService {

    @Autowired
    private CountryDao countryDao;

    @Override
    public Country getById(int id) {
        return countryDao.getById(id);
    }

    @Override
    public List<Country> getCountriesByMovieId(int id) {
        return countryDao.getCountriesByMovieId(id);
    }

    @Override
    public List<Country> getAll() {
        return countryDao.getAll();
    }
}
