package com.pashiur.moviestore.service;

import com.pashiur.moviestore.entity.Genre;

import java.util.List;

public interface GenreService {
    Genre getById(int id);

    List<Genre> getGenresByMovieId(int id);

    List<Genre> getAll();
}
