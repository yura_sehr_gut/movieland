package com.pashiur.moviestore.service;

import com.pashiur.moviestore.entity.Movie;
import com.pashiur.moviestore.filters.MovieFilter;

import java.util.List;

public interface MovieService {
    List<Movie> getAll(String ratingOrder, String priceOrder);

    Movie getById(int id);

    List<Movie> search(MovieFilter movieFilter);
}
