package com.pashiur.moviestore.service;


import com.pashiur.moviestore.entity.User;

public interface UserService {
    User getById(int id);
}
