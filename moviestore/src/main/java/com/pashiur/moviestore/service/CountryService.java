package com.pashiur.moviestore.service;

import com.pashiur.moviestore.entity.Country;

import java.util.List;

public interface CountryService {
    Country getById(int id);

    List<Country> getCountriesByMovieId(int id);

    List<Country> getAll();
}
