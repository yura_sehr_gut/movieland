package com.pashiur.moviestore.service.impl;

import com.pashiur.moviestore.dao.GenreDao;
import com.pashiur.moviestore.entity.Genre;
import com.pashiur.moviestore.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GenreServiceImpl implements GenreService {
    @Autowired
    private GenreDao genreDao;

    @Override
    public Genre getById(int id) {
        return genreDao.getById(id);
    }

    @Override
    public List<Genre> getGenresByMovieId(int id) {
        return genreDao.getGenresByMovieId(id);
    }

    @Override
    public List<Genre> getAll() {
        return genreDao.getAll();
    }
}
