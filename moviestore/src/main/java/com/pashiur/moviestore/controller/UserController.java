package com.pashiur.moviestore.controller;

import com.pashiur.moviestore.dto.UserView;
import com.pashiur.moviestore.entity.User;
import com.pashiur.moviestore.service.UserService;
import com.pashiur.moviestore.util.JsonJacksonConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/v1/user")
public class UserController {
    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private UserService userService;

    @Autowired
    private JsonJacksonConverter jsonJacksonConverter;

    @RequestMapping(value = "/{id}", produces = "text/plain;charset=UTF-8")
    @ResponseBody
    public String getById(@PathVariable int id) {
        log.info("Sending request to get user with id = {}", id);
        long startTime = System.currentTimeMillis();
        User user = userService.getById(id);
        String userJson = jsonJacksonConverter.toJson(new UserView(user));
        log.info("User is received. It took {} ms", System.currentTimeMillis() - startTime);
        return userJson;
    }
}
