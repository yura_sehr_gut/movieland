package com.pashiur.moviestore.controller;

import com.pashiur.moviestore.dto.MovieViewById;
import com.pashiur.moviestore.entity.Movie;
import com.pashiur.moviestore.filters.MovieFilter;
import com.pashiur.moviestore.service.MovieService;
import com.pashiur.moviestore.util.JsonJacksonConverter;
import com.pashiur.moviestore.util.Transformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/v1/movie")
public class MovieController {
    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private MovieService movieService;

    @Autowired
    private JsonJacksonConverter jsonJacksonConverter;

    @RequestMapping(produces = "text/plain;charset=UTF-8")
    @ResponseBody
    public String getAll(@RequestParam(value = "rating", required = false) String ratingOrder,
                         @RequestParam(value = "price", required = false) String priceOrder) {
        log.info("Sending request to get all movies");
        long startTime = System.currentTimeMillis();
        String moviesJson = jsonJacksonConverter.toJson(Transformer.toMovieViews(movieService.getAll(ratingOrder, priceOrder)));
        log.info("Movies are received. It took {} ms", System.currentTimeMillis() - startTime);
        return moviesJson;
    }

    @RequestMapping(value = "/{id}", produces = "text/plain;charset=UTF-8")
    @ResponseBody
    public String getById(@PathVariable int id) {
        log.info("Sending request to get movie with id = {}", id);
        long startTime = System.currentTimeMillis();
        Movie movie = movieService.getById(id);
        MovieViewById movieViewById = new MovieViewById(movie);
        String movieJson = jsonJacksonConverter.toJson(movieViewById);
        log.info("Movie is received. It took {} ms", System.currentTimeMillis() - startTime);
        return movieJson;
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    @ResponseBody
    public ResponseEntity<String> search(@RequestBody String json) {
        log.info("Sending request to get filtered movies");
        long startTime = System.currentTimeMillis();
        MovieFilter movieFilter = jsonJacksonConverter.toObject(json, MovieFilter.class);
        List<Movie> movies = movieService.search(movieFilter);
        String jsonResponse = jsonJacksonConverter.toJson(movies);
        log.info("Movies are received. It took {} ms", System.currentTimeMillis() - startTime);
        return new ResponseEntity<>(jsonResponse, HttpStatus.OK);
    }
}
