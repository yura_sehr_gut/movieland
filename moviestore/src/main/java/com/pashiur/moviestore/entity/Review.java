package com.pashiur.moviestore.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Review {
    @JsonIgnore
    private Movie movie;
    private User user;
    private String feedback;

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    @Override
    public String toString() {
        return "Review{" +
                "movie=" + movie.getName() +
                ", user=" + user +
                ", feedback='" + feedback + '\'' +
                '}';
    }
}
