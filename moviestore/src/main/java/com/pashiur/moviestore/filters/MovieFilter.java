package com.pashiur.moviestore.filters;

public class MovieFilter {
    private String genre;
    private String name;
    @Column(name = "name_eng")
    private String nameEng;
    @Column(name = "movie_year")
    private String movieYear;
    private String country;

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameEng() {
        return nameEng;
    }

    public void setNameEng(String nameEng) {
        this.nameEng = nameEng;
    }

    public String getMovieYear() {
        return movieYear;
    }

    public void setMovieYear(String movieYear) {
        this.movieYear = movieYear;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "MovieFilter{" +
                "genre='" + genre + '\'' +
                ", name='" + name + '\'' +
                ", nameEng='" + nameEng + '\'' +
                ", movieYear='" + movieYear + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
