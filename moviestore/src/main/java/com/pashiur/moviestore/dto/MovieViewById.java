package com.pashiur.moviestore.dto;

import com.pashiur.moviestore.entity.Movie;
import com.pashiur.moviestore.util.Transformer;

import java.util.List;

public class MovieViewById {
    private int id;
    private String name;
    private String nameEng;
    private int year;
    private double ratting;
    private double price;
    private String description;
    private String genre;
    private String country;
    private List<ReviewView> reviews;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameEng() {
        return nameEng;
    }

    public void setNameEng(String nameEng) {
        this.nameEng = nameEng;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getRatting() {
        return ratting;
    }

    public void setRatting(double ratting) {
        this.ratting = ratting;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<ReviewView> getReviews() {
        return reviews;
    }

    public void setReviews(List<ReviewView> reviews) {
        this.reviews = reviews;
    }

    @Override
    public String toString() {
        return "MovieViewById{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", nameEng='" + nameEng + '\'' +
                ", year=" + year +
                ", ratting=" + ratting +
                ", price=" + price +
                ", description='" + description + '\'' +
                ", genre='" + genre + '\'' +
                ", country='" + country + '\'' +
                ", reviews=" + reviews +
                '}';
    }

    public MovieViewById(Movie movie) {
        this.id = movie.getId();
        this.name = movie.getName();
        this.nameEng = movie.getNameEng();
        this.year = movie.getYear();
        this.ratting = movie.getRatting();
        this.price = movie.getPrice();
        this.description = movie.getDescription();
        this.genre = Transformer.toStringGenres(movie.getGenre());
        this.country = Transformer.toStringCountries(movie.getCountry());
        this.reviews = Transformer.toReviewViews(movie.getReviews(), 2);
    }
}
