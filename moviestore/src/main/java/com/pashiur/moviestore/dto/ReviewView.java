package com.pashiur.moviestore.dto;

import com.pashiur.moviestore.entity.Review;
import com.pashiur.moviestore.util.Transformer;

public class ReviewView {
    private String userName;
    private String userURL;
    private String feedback;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserURL() {
        return userURL;
    }

    public void setUserURL(String userURL) {
        this.userURL = userURL;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    @Override
    public String toString() {
        return "ReviewView{" +
                "userName='" + userName + '\'' +
                ", userURL='" + userURL + '\'' +
                ", feedback='" + feedback + '\'' +
                '}';
    }

    public ReviewView(Review review) {
        this.userName = review.getUser().getName();
        this.userURL = Transformer.toUserUrl(review.getUser());
        this.feedback = review.getFeedback();
    }
}
