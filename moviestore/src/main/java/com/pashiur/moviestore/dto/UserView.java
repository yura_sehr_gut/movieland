package com.pashiur.moviestore.dto;

import com.pashiur.moviestore.entity.User;

public class UserView {
    private int id;
    private String name;
    private String mail;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    @Override
    public String toString() {
        return "UserView{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", mail='" + mail + '\'' +
                '}';
    }

    public UserView(User user) {
        this.id = user.getId();
        this.name = user.getName();
        this.mail = user.getMail();
    }
}
