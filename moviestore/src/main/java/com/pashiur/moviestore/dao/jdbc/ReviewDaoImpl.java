package com.pashiur.moviestore.dao.jdbc;

import com.pashiur.moviestore.dao.ReviewDao;
import com.pashiur.moviestore.dao.jdbc.mapper.ReviewRowMapper;
import com.pashiur.moviestore.entity.Review;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ReviewDaoImpl implements ReviewDao {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private String getReviewsByMovieIdSQL;

    private ReviewRowMapper reviewRowMapper = new ReviewRowMapper();

    @Override
    public List<Review> getReviewsByMovieId(int movieId) {
        log.info("Start query to get user with id {} from DB", movieId);
        long startTime = System.currentTimeMillis();
        List<Review> reviews = jdbcTemplate.query(getReviewsByMovieIdSQL, new Object[]{movieId}, reviewRowMapper);
        log.info("Finish query to get user with id {} from DB. It took {} ms", movieId, System.currentTimeMillis() - startTime);
        return reviews;
    }
}
