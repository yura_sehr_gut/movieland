package com.pashiur.moviestore.dao;

import com.pashiur.moviestore.entity.User;

public interface UserDao {
    User getById(int id);
}
