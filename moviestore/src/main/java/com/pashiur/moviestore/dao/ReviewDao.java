package com.pashiur.moviestore.dao;

import com.pashiur.moviestore.entity.Review;

import java.util.List;

public interface ReviewDao {
    List<Review> getReviewsByMovieId(int movieId);
}
