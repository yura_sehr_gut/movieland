package com.pashiur.moviestore.dao.jdbc.mapper;


import com.pashiur.moviestore.dao.CountryDao;
import com.pashiur.moviestore.dao.GenreDao;
import com.pashiur.moviestore.entity.Movie;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class MovieRowMapper implements RowMapper<Movie> {

    @Override
    public Movie mapRow(ResultSet resultSet, int i) throws SQLException {
        Movie movie = new Movie();
        movie.setId(resultSet.getInt("id"));
        movie.setName(resultSet.getString("name"));
        movie.setNameEng(resultSet.getString("name_eng"));
        movie.setYear(resultSet.getInt("movie_year"));
        movie.setDescription(resultSet.getString("description"));
        movie.setRatting(resultSet.getDouble("rating"));
        movie.setPrice(resultSet.getDouble("price"));
        return movie;
    }
}
