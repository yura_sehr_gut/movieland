package com.pashiur.moviestore.dao;

import com.pashiur.moviestore.entity.Country;

import java.util.List;

public interface CountryDao {
    Country getById(int id);

    List<Country> getCountriesByMovieId(int id);

    List<Country> getAll();
}
