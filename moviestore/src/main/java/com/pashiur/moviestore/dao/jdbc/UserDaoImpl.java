package com.pashiur.moviestore.dao.jdbc;

import com.pashiur.moviestore.dao.UserDao;
import com.pashiur.moviestore.dao.jdbc.mapper.UserRowMapper;
import com.pashiur.moviestore.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class UserDaoImpl implements UserDao {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private String getUserByIdSQL;

    private UserRowMapper userRowMapper = new UserRowMapper();

    @Override
    public User getById(int id) {
        log.info("Start query to get user with id {} from DB", id);
        long startTime = System.currentTimeMillis();
        User user = jdbcTemplate.queryForObject(getUserByIdSQL, new Object[]{id}, userRowMapper);
        log.info("Finish query to get user with id {} from DB. It took {} ms", id, System.currentTimeMillis() - startTime);
        return user;
    }
}
