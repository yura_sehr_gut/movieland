package com.pashiur.moviestore.dao.jdbc;

import com.pashiur.moviestore.dao.CountryDao;
import com.pashiur.moviestore.dao.jdbc.mapper.CountryRowMapper;
import com.pashiur.moviestore.entity.Country;
import com.pashiur.moviestore.entity.Genre;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CountryDaoImpl implements CountryDao {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private String getCountryByIdSQL;

    @Autowired
    private String getCountriesByMovieIdSQL;

    @Autowired
    private String getAllCountriesSQL;

    private CountryRowMapper countryRowMapper = new CountryRowMapper();

    @Override
    public Country getById(int id) {
        log.info("Start query to get country with id {} from DB", id);
        long startTime = System.currentTimeMillis();
        Country country = jdbcTemplate.queryForObject(getCountryByIdSQL, new Object[]{id}, countryRowMapper);
        log.info("Finish query to get country with id {} from DB. It took {} ms", id, System.currentTimeMillis() - startTime);
        return country;
    }

    @Override
    public List<Country> getCountriesByMovieId(int id) {
        log.info("Start query to get countries for movie with id {} from DB", id);
        long startTime = System.currentTimeMillis();
        List<Country> countries = jdbcTemplate.query(getCountriesByMovieIdSQL, new Object[]{id}, countryRowMapper);
        log.info("Finish query to get countries for movie with id {} from DB. It took {} ms", id, System.currentTimeMillis() - startTime);
        return countries;
    }

    @Override
    public List<Country> getAll() {
        log.info("Start query to get all countries from DB");
        long startTime = System.currentTimeMillis();
        List<Country> countries = jdbcTemplate.query(getAllCountriesSQL, countryRowMapper);
        log.info("Finish query to get all countries from DB", System.currentTimeMillis() - startTime);
        return countries;
    }
}
