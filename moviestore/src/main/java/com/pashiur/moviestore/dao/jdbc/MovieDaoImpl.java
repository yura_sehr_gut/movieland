package com.pashiur.moviestore.dao.jdbc;

import com.pashiur.moviestore.dao.MovieDao;
import com.pashiur.moviestore.dao.jdbc.mapper.MovieRowMapper;
import com.pashiur.moviestore.entity.Movie;
import com.pashiur.moviestore.filters.MovieFilter;
import com.pashiur.moviestore.util.SqlFilterGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MovieDaoImpl implements MovieDao {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private String getAllMoviesSQL;

    @Autowired
    private String getMoviesByIdSQL;

    @Autowired
    private String getFilteredMoviesSQL;

    private MovieRowMapper movieRowMapper = new MovieRowMapper();

    @Override
    public List<Movie> getAll() {
        log.info("Start query to get all movies with id {} from DB");
        long startTime = System.currentTimeMillis();
        List<Movie> movies = jdbcTemplate.query(getAllMoviesSQL, movieRowMapper);
        log.info("Finish query to get all movies with id {} from DB. It took {} ms", System.currentTimeMillis() - startTime);
        return movies;
    }

    @Override
    public Movie getById(int id) {
        log.info("Start query to get movie with id {} from DB", id);
        long startTime = System.currentTimeMillis();
        Movie movie = jdbcTemplate.queryForObject(getMoviesByIdSQL, new Object[]{id}, movieRowMapper);
        log.info("Finish query to get user with id {} from DB. It took {} ms", id, System.currentTimeMillis() - startTime);
        return movie;
    }

    @Override
    public List<Movie> search(MovieFilter movieFilter) {
        log.info("Start query to get all movies with filter {} from DB", movieFilter);
        long startTime = System.currentTimeMillis();
        List<Movie> movies = jdbcTemplate.query(getFilteredMoviesSQL + SqlFilterGenerator.buildSqlFilter(movieFilter) + ";", movieRowMapper);
        log.info("Finish query to get all movies with filter {} from DB. It took {} ms", System.currentTimeMillis() - startTime);
        return movies;
    }

}
