package com.pashiur.moviestore.dao;

import com.pashiur.moviestore.entity.Genre;

import java.util.List;

public interface GenreDao {
    Genre getById(int id);

    List<Genre> getGenresByMovieId(int id);

    List<Genre> getAll();
}
