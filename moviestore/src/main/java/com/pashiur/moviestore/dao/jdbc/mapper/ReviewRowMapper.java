package com.pashiur.moviestore.dao.jdbc.mapper;

import com.pashiur.moviestore.entity.Movie;
import com.pashiur.moviestore.entity.Review;
import com.pashiur.moviestore.entity.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ReviewRowMapper implements RowMapper<Review> {
    @Override
    public Review mapRow(ResultSet resultSet, int i) throws SQLException {
        Review review = new Review();
        Movie movie = new Movie();
        User user = new User();
        movie.setId(resultSet.getInt("movie_id"));
        user.setId(resultSet.getInt("user_id"));
        review.setMovie(movie);
        review.setUser(user);
        review.setFeedback(resultSet.getString("feedback"));
        return review;
    }
}
