package com.pashiur.moviestore.dao.jdbc;

import com.pashiur.moviestore.dao.GenreDao;
import com.pashiur.moviestore.dao.jdbc.mapper.GenreRowMapper;
import com.pashiur.moviestore.entity.Genre;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class GenreDaoImpl implements GenreDao {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private String getGenreByIdSQL;

    @Autowired
    private String getGenresByMovieIdSQL;

    @Autowired
    private String getAllGenresSQL;

    private GenreRowMapper genreRowMapper = new GenreRowMapper();

    @Override
    public Genre getById(int id) {
        log.info("Start query to get genre with id {} from DB", id);
        long startTime = System.currentTimeMillis();
        Genre genre = jdbcTemplate.queryForObject(getGenreByIdSQL, new Object[]{id}, genreRowMapper);
        log.info("Finish query to get genre with id {} from DB. It took {} ms", id, System.currentTimeMillis() - startTime);
        return genre;
    }


    @Override
    public List<Genre> getGenresByMovieId(int id) {
        log.info("Start query to get genres for movie with id {} from DB", id);
        long startTime = System.currentTimeMillis();
        List<Genre> genres = jdbcTemplate.query(getGenresByMovieIdSQL, new Object[]{id}, genreRowMapper);
        log.info("Finish query to get genres for movie with id {} from DB. It took {} ms", id, System.currentTimeMillis() - startTime);
        return genres;
    }

    @Override
    public List<Genre> getAll() {
        log.info("Start query to get all genres from DB");
        long startTime = System.currentTimeMillis();
        List<Genre> genres = jdbcTemplate.query(getAllGenresSQL, genreRowMapper);
        log.info("Finish query to get all genres from DB", System.currentTimeMillis() - startTime);
        return genres;
    }
}
