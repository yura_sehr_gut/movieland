package com.pashiur.moviestore.dao;

import com.pashiur.moviestore.entity.Movie;
import com.pashiur.moviestore.filters.MovieFilter;

import java.util.List;

public interface MovieDao {
    List<Movie> getAll();

    Movie getById(int id);

    List<Movie> search(MovieFilter movieFilter);
}
