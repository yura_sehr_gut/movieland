package com.pashiur.moviestore.util;

import com.pashiur.moviestore.entity.Movie;
import com.pashiur.moviestore.util.enums.Sorting;

import java.util.Comparator;

public class MovieComparator implements Comparator<Movie> {
    private Sorting ratingOrder;
    private Sorting priceOrder;

    public MovieComparator(String ratingOrder, String priceOrder) {
        if (ratingOrder != null) {
            this.ratingOrder = Sorting.valueOf(ratingOrder.toUpperCase());
        }
        if (priceOrder != null) {
            this.priceOrder = Sorting.valueOf(priceOrder.toUpperCase());
        }
    }

    @Override
    public int compare(Movie o1, Movie o2) {
        int result = 0;
        if (Sorting.ASC == ratingOrder) {
            result = o1.getRatting().compareTo(o2.getRatting());
        } else if (Sorting.DESC == ratingOrder) {
            result = -o1.getRatting().compareTo(o2.getRatting());
        }
        if (result == 0) {
            if (Sorting.ASC == priceOrder) {
                result = o1.getPrice().compareTo(o2.getPrice());
            } else if (Sorting.DESC == priceOrder) {
                result = -o1.getPrice().compareTo(o2.getPrice());
            }
        }
        return result;
    }
}
