package com.pashiur.moviestore.util.enums;

public enum Sorting {
    ASC, DESC
}
