package com.pashiur.moviestore.util;

import com.pashiur.moviestore.filters.Column;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;

public class SqlFilterGenerator {
    public static final String trueCondition = " where 1=1 ";

    public static String buildSqlFilter(Object o) {
        Logger log = LoggerFactory.getLogger(SqlFilterGenerator.class);
        log.info("Start building SQL filter {} to json ", o);
        Field[] fields = o.getClass().getDeclaredFields();
        StringBuilder stringBuilder = new StringBuilder();
        String fieldName;
        stringBuilder.append(trueCondition);
        for (Field field : fields) {
            try {
                field.setAccessible(true);
                if (field.get(o) != null) {
                    if (field.isAnnotationPresent(Column.class)) {
                        fieldName = field.getAnnotation(Column.class).name();
                    } else {
                        fieldName = field.getName();
                    }
                    stringBuilder.append("and " + fieldName + " like '%" + field.get(o) + "%' ");
                }
                field.setAccessible(false);
            } catch (IllegalAccessException e) {
                log.warn("Sql filter generator exception:" + e.getStackTrace());
            }
        }
        log.info("End building SQL filter {} to json ", o);
        return stringBuilder.toString();
    }
}
