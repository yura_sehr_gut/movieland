package com.pashiur.moviestore.util;

import com.pashiur.moviestore.dto.MovieView;
import com.pashiur.moviestore.dto.ReviewView;
import com.pashiur.moviestore.entity.*;

import java.util.ArrayList;
import java.util.List;

public class Transformer {

    public static List<ReviewView> toReviewViews(List<Review> reviews, int limit) {
        List<ReviewView> reviewViews = new ArrayList<>();
        for (int i = 0; i < (reviews.size() > limit ? limit : reviews.size()); i++) {
            reviewViews.add(new ReviewView(reviews.get(i)));
        }
        return reviewViews;
    }

    public static String toUserUrl(User user) {
        return "v1/user/" + user.getId();
    }

    public static String toStringGenres(List<Genre> genres) {
        StringBuilder tempGenres = new StringBuilder();
        for (Genre genre : genres) {
            tempGenres.append(", ");
            tempGenres.append(genre.getName());
        }
        return tempGenres.toString().substring(2, tempGenres.toString().length());
    }

    public static String toStringCountries(List<Country> countries) {
        StringBuilder tempCountries = new StringBuilder();
        for (Country country : countries) {
            tempCountries.append(", ");
            tempCountries.append(country.getName());
        }
        return tempCountries.toString().substring(2, tempCountries.toString().length());
    }

    public static List<MovieView> toMovieViews(List<Movie> movies) {
        List<MovieView> movieViews = new ArrayList<>();
        for (Movie moview : movies) {
            movieViews.add(new MovieView(moview));
        }
        return movieViews;
    }
}
