package com.pashiur.moviestore.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class JsonJacksonConverter {
    private final Logger log = LoggerFactory.getLogger(getClass());
    // thread-safe
    private ObjectMapper objectMapper = new ObjectMapper();

    public String toJson(Object obj) {
        String json = "";
        log.info("Start transform object {} to json ", obj);
        long startTime = System.currentTimeMillis();
        try {
            json = objectMapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            log.warn("Jackson converter exception:" + e.getStackTrace());
        }
        long time = System.currentTimeMillis() - startTime;
        log.info("Json {} is received. It took {} ms", json, time);
        return json;
    }

    public <T> T toObject(String jsonInString, Class<T> tClass) {
        T o = null;
        log.info("Start transform jsonInString {} to json ", jsonInString);
        long startTime = System.currentTimeMillis();
        try {
            o = objectMapper.readValue(jsonInString, tClass);
        } catch (IOException e) {
            log.warn("Jackson converter exception:" + e.getStackTrace());
        }
        long time = System.currentTimeMillis() - startTime;
        log.info("MovieFilter {} is received. It took {} ms", time);
        return o;
    }
}
