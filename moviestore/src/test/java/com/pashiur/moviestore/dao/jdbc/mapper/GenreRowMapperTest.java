package com.pashiur.moviestore.dao.jdbc.mapper;

import com.pashiur.moviestore.entity.Genre;
import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GenreRowMapperTest {

    @Test
    public void testMapRowWithProperGenre() throws SQLException {
        ResultSet resultSet = mock(ResultSet.class);

        when(resultSet.getInt(any())).thenReturn(1);
        when(resultSet.getString(any())).thenReturn("Test genre");

        GenreRowMapper genreRowMapper = new GenreRowMapper();
        Genre actualGenre = genreRowMapper.mapRow(resultSet,0);
        assertEquals(actualGenre.getId(),1);
        assertEquals(actualGenre.getName(),"Test genre");
    }
}
