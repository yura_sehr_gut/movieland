package com.pashiur.moviestore.dao.jdbc.mapper;

import com.pashiur.moviestore.entity.Country;
import com.pashiur.moviestore.entity.Genre;
import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CountryRowMapperTest {

    @Test
    public void testMapRowWithProperCountry() throws SQLException {
        ResultSet resultSet = mock(ResultSet.class);

        when(resultSet.getInt(any())).thenReturn(1);
        when(resultSet.getString(any())).thenReturn("Test country");

        CountryRowMapper countryRowMapper = new CountryRowMapper();
        Country actualCountry = countryRowMapper.mapRow(resultSet, 0);
        assertEquals(actualCountry.getId(), 1);
        assertEquals(actualCountry.getName(), "Test country");
    }
}
