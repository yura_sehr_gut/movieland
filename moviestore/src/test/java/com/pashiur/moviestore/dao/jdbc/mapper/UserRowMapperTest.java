package com.pashiur.moviestore.dao.jdbc.mapper;

import com.pashiur.moviestore.entity.User;
import org.junit.Test;

import javax.jws.soap.SOAPBinding;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserRowMapperTest {

    @Test
    public void testMapRowWithProperGenre() throws SQLException {
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.getInt(any())).thenReturn(1);
        when(resultSet.getString(any())).thenReturn("Test user").thenReturn("Test mail").thenReturn("Test password");

        UserRowMapper userRowMapper = new UserRowMapper();
        User actualUser = userRowMapper.mapRow(resultSet, 0);
        assertEquals(actualUser.getId(), 1);
        assertEquals(actualUser.getName(), "Test user");
        assertEquals(actualUser.getMail(), "Test mail");
        assertEquals(actualUser.getPasword(), "Test password");
    }
}
