package com.pashiur.moviestore.dao.jdbc.mapper;

import com.pashiur.moviestore.entity.Review;
import com.pashiur.moviestore.entity.User;
import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ReviewRowMapperTest {

    @Test
    public void testMapRowWithProperReview() throws SQLException {
        ResultSet resultSet = mock(ResultSet.class);

        when(resultSet.getInt(any())).thenReturn(1).thenReturn(2);
        when(resultSet.getString(any())).thenReturn("Test feedback");

        ReviewRowMapper reviewRowMapper = new ReviewRowMapper();
        Review actualReview = reviewRowMapper.mapRow(resultSet, 0);
        assertEquals(actualReview.getMovie().getId(), 1);
        assertEquals(actualReview.getUser().getId(), 2);
        assertEquals(actualReview.getFeedback(), "Test feedback");
    }
}
