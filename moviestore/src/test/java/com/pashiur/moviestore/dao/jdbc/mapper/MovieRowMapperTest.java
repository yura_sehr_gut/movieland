package com.pashiur.moviestore.dao.jdbc.mapper;

        import com.pashiur.moviestore.entity.Movie;
        import org.junit.Test;

        import java.sql.ResultSet;
        import java.sql.SQLException;

        import static org.junit.Assert.assertEquals;
        import static org.mockito.Matchers.any;
        import static org.mockito.Mockito.mock;
        import static org.mockito.Mockito.when;


public class MovieRowMapperTest {
    @Test
    public void testMapRowWithProperMovie() throws SQLException {
        ResultSet resultSet = mock(ResultSet.class);

        when(resultSet.getInt(any())).thenReturn(1).thenReturn(2016);
        when(resultSet.getDouble(any())).thenReturn(1.1).thenReturn(999.99);
        when(resultSet.getString(any())).thenReturn("Test movie name").thenReturn("Test movie name_eng").thenReturn("Test movie description");

        MovieRowMapper movieRowMapper = new MovieRowMapper();
        Movie actualMovie = movieRowMapper.mapRow(resultSet, 0);
        assertEquals(actualMovie.getId(), 1);
        assertEquals(actualMovie.getName(), "Test movie name");
        assertEquals(actualMovie.getNameEng(), "Test movie name_eng");
        assertEquals(actualMovie.getYear(), 2016);
        assertEquals(actualMovie.getDescription(), "Test movie description");
        assertEquals(actualMovie.getRatting(), 1.1, 0);
        assertEquals(actualMovie.getPrice(), 999.99, 0);
    }
}

