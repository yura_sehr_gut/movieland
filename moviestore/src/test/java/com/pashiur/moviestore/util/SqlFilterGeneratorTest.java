package com.pashiur.moviestore.util;

import com.pashiur.moviestore.filters.MovieFilter;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class SqlFilterGeneratorTest {

    @Test
    public void testBuildSqlFilter() throws Exception {
        MovieFilter movieFilter = new MovieFilter();
        assertEquals(SqlFilterGenerator.buildSqlFilter(movieFilter), " where 1=1 ");
        movieFilter.setName("Test name");
        assertEquals(SqlFilterGenerator.buildSqlFilter(movieFilter), " where 1=1 and name like '%Test name%' ");
        movieFilter.setMovieYear("2016");
        assertEquals(SqlFilterGenerator.buildSqlFilter(movieFilter), " where 1=1 and name like '%Test name%' and movie_year like '%2016%' ");
    }
}