package com.pashiur.moviestore.util;

import com.pashiur.moviestore.entity.Movie;
import org.junit.Test;

import static org.junit.Assert.*;

public class MovieComparatorTest {

    @Test
    public void testCompare() {
        Movie movie1 = new Movie();
        movie1.setRatting((double) 2);
        Movie movie2 = new Movie();
        movie2.setRatting((double) 3);
        MovieComparator movieComparator = new MovieComparator("asc", null);
        assertTrue(movieComparator.compare(movie1, movie2) < 0);
        movieComparator = new MovieComparator("desc", null);
        assertTrue(movieComparator.compare(movie1, movie2) > 0);
        movie1.setPrice(10.12);
        movie2.setPrice(777.56);
        movieComparator = new MovieComparator(null, "asc");
        assertTrue(movieComparator.compare(movie1, movie2) < 0);
        movieComparator = new MovieComparator(null, "desc");
        assertTrue(movieComparator.compare(movie1, movie2) > 0);
    }
}