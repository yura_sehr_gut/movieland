import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Yurij_000 on 6/7/2016.
 */
public class GenreLoader {
    private static final String GENRES = "resources/genre.txt";

    public static void main(String[] args) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(GENRES))) {
            String message;
            while ((message = bufferedReader.readLine()) != null) {
                System.out.println("insert into genres (name) values  ('" + message + "');");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
