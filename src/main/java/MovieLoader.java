import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by Yurij_000 on 6/7/2016.
 */
public class MovieLoader {
    private static final String MOVIE = "resources/movie.txt";

    public static void main(String[] args) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(MOVIE))) {
            HashSet<String> countries = new HashSet<>();
            Map<String, ArrayList<String>> movieCountry = new HashMap<>();
            Map<String, ArrayList<String>> movieGenre = new HashMap<>();
            String nameMovie = "";
            String nameMovieEng = "";
            int indexOfDelimiter = 0;
            String message;
            String sql = "'";
            String sql_template = "insert into movies (name, name_eng, movie_year, description, rating, price) values (";
            Byte steps = 0;
            while ((message = bufferedReader.readLine()) != null) {
                //System.out.println(message);
                steps++;
                if (steps == 1) {
                    indexOfDelimiter = message.indexOf('/');
                    nameMovie = message.substring(0, indexOfDelimiter);
                    nameMovieEng = message.substring(indexOfDelimiter + 1).replace("'", "''");
                    sql = sql + nameMovie + "','" + nameMovieEng + "','";
                } else if (steps == 2) {
                    sql = sql + message + "','";
                } else if (steps == 3) {
                    ArrayList<String> tempCountries = new ArrayList<>();
                    for (String country : message.split(",")) {
                        //System.out.println("country: " + country.trim());
                        tempCountries.add(country.trim());
                        countries.add(country.trim());
                    }
                    movieCountry.put(nameMovie, tempCountries);
                } else if (steps == 4) {
                    ArrayList<String> tempGenre = new ArrayList<>();
                    for (String genre : message.split(",")) {
                        tempGenre.add(genre.trim());
                    }
                    movieGenre.put(nameMovie, tempGenre);
                } else if (steps == 5) {
                    sql = sql + message + "','";
                } else if (steps == 6) {
                    System.out.println(message.substring(7));
                    sql = sql + message.substring(7) + "','";
                } else if (steps == 7) {
                    System.out.println(message.substring(6));
                    sql = sql + message.substring(6) + "','";
                } else if (steps == 8) {
                    steps = 0;
                    System.out.println(sql_template + sql + ");");
                    sql = "'";
                }
            }
            for (String varCountry : countries) {
                System.out.println("insert into countries(name) values('" + varCountry + "');");
            }

/*            for (String movie : movieCountry.keySet()) {
                System.out.println(movie);
                for (String country : movieCountry.get(movie)) {
                    System.out.println(country);
                    System.out.println("INSERT into movie_country (movie_id, country_id) VALUES ((select id from movie where name = '" + nameMovie + "'), (select id from countries where type = '" + country + "')); ");
                }
            }*/

            for (String movie : movieGenre.keySet()) {
                //System.out.println(movie);
                for (String genre : movieGenre.get(movie)) {
                    //System.out.println(country);
                    System.out.println("INSERT into movie_genre (movie_id, genre_id) VALUES ((select id from movie where name = '" + movie + "'), (select id from genres where name = '" + genre + "')); ");
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
