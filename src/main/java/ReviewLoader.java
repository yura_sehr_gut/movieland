import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by Yurij_000 on 6/7/2016.
 */
public class ReviewLoader {
    private static final String REVIEW = "resources/review.txt";

    public static void main(String[] args) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(REVIEW))) {
            String message;
            String sql = "";
            String movie = "";
            String user = "";
            Byte steps = 0;
            while ((message = bufferedReader.readLine()) != null) {
                //System.out.println(message);
                steps++;
                if (steps == 1) {
                    movie = message;
                } else if (steps == 2) {
                    user = message;
                } else if (steps == 3) {
                    System.out.println("INSERT into reviews (movie_id, user_id, feedback) VALUES ((select id from movie where name = '" + movie + "'), (select id from users where name = '" + user + "'), '" + message + "');");
                } else if (steps == 4) {
                    steps = 0;
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
