import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Created by Yurij_000 on 6/7/2016.
 */
public class PostgreSQLJDBC {
    private static final String GENRES = "resources/genre.txt";
    private static final String USERS = "resources/user.txt";
    private static final String MOVIES = "resources/movie.txt";
    private static final String REVIEW = "resources/review.txt";

    public static void main(String args[]) {
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres",
                    "postgres", "Yura1_Daria1");
            System.out.println("Opened database successfully");
            c.setAutoCommit(false);

            stmt = c.createStatement();
            String sql_del = "delete from genres";
            stmt.executeUpdate(sql_del);
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(GENRES))) {
                String message;
                while ((message = bufferedReader.readLine()) != null) {
                    //System.out.println("insert into genres (name) values  ('" + message + "');");
                    String sql = "insert into genres (name) values  ('" + message + "');";
                    stmt.executeUpdate(sql);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            sql_del = "delete from users";
            stmt.executeUpdate(sql_del);
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(USERS))) {
                String message;
                String sql = "'";
                String sql_template = "insert into users (name, mail, password) values (";
                Byte steps = 0;
                while ((message = bufferedReader.readLine()) != null) {
                    //System.out.println("insert into genres (name) values  ('" + message + "');");
                    sql = sql + (sql.length() == 1 ? "" : ",'") + message + "'";
                    steps++;
                    if (steps == 3) {
                        sql = sql_template + sql + ");";
                        stmt.executeUpdate(sql);
                    }
                    if (steps == 4) {
                        sql = "'";
                        steps = 0;
                    }
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            sql_del = "delete from movies";
            stmt.executeUpdate(sql_del);
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(MOVIES))) {
                HashSet<String> countries = new HashSet<>();
                Map<String, ArrayList<String>> movieCountry = new HashMap<>();
                Map<String, ArrayList<String>> movieGenre = new HashMap<>();
                String nameMovie = "";
                String nameMovieEng = "";
                int indexOfDelimiter = 0;
                String message;
                String sql = "'";
                String sql_template = "insert into movies (name, name_eng, movie_year, description, rating, price) values (";
                Byte steps = 0;
                while ((message = bufferedReader.readLine()) != null) {
                    steps++;
                    if (steps == 1) {
                        indexOfDelimiter = message.indexOf('/');
                        nameMovie = message.substring(0, indexOfDelimiter);
                        nameMovieEng = message.substring(indexOfDelimiter + 1).replace("'", "''");
                        sql = sql + nameMovie + "','" + nameMovieEng + "','";
                    } else if (steps == 2) {
                        sql = sql + message + "','";
                    } else if (steps == 3) {
                        ArrayList<String> tempCountries = new ArrayList<>();
                        for (String country : message.split(",")) {
                            tempCountries.add(country.trim());
                            countries.add(country.trim());
                        }
                        movieCountry.put(nameMovie, tempCountries);
                    } else if (steps == 4) {
                        ArrayList<String> tempGenre = new ArrayList<>();
                        for (String genre : message.split(",")) {
                            tempGenre.add(genre.trim());
                        }
                        movieGenre.put(nameMovie, tempGenre);
                    } else if (steps == 5) {
                        sql = sql + message + "','";
                    } else if (steps == 6) {
                        sql = sql + message.substring(7) + "','";
                    } else if (steps == 7) {
                        sql = sql + message.substring(6) + "'";
                    } else if (steps == 8) {
                        steps = 0;
                        sql = sql_template + sql + ");";
                        //System.out.println(sql);
                        stmt.executeUpdate(sql);
                        sql = "'";
                    }
                }
                sql_del = "delete from countries";
                stmt.executeUpdate(sql_del);
                for (String varCountry : countries) {
                    sql = "insert into countries(name) values('" + varCountry + "');";
                    stmt.executeUpdate(sql);
                }
                sql_del = "delete from movie_country";
                stmt.executeUpdate(sql_del);
                for (String movie : movieCountry.keySet()) {
                    for (String country : movieCountry.get(movie)) {
                        sql = "INSERT into movie_country (movie_id, country_id) VALUES ((select id from movies where name = '" + movie + "'), (select id from countries where name = '" + country + "')); ";
                        stmt.executeUpdate(sql);
                    }
                }
                sql_del = "delete from movie_genre";
                stmt.executeUpdate(sql_del);
                for (String movie : movieGenre.keySet()) {
                    for (String genre : movieGenre.get(movie)) {
                        sql = "INSERT into movie_genre (movie_id, genre_id) VALUES ((select id from movies where name = '" + movie + "'), (select id from genres where name = '" + genre + "')); ";
                        stmt.executeUpdate(sql);
                    }
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            sql_del = "delete from reviews";
            stmt.executeUpdate(sql_del);
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(REVIEW))) {
                String message;
                String sql = "";
                String movie = "";
                String user = "";
                Byte steps = 0;
                while ((message = bufferedReader.readLine()) != null) {
                    steps++;
                    if (steps == 1) {
                        movie = message;
                    } else if (steps == 2) {
                        user = message;
                    } else if (steps == 3) {
                        sql = "INSERT into reviews (movie_id, user_id, feedback) VALUES ((select id from movies where name = '" + movie + "'), (select id from users where name = '" + user + "'), '" + message + "');";
                        stmt.executeUpdate(sql);
                    } else if (steps == 4) {
                        steps = 0;
                    }
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            stmt.close();
            c.commit();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        // System.out.println("Table created successfully");
    }
}
