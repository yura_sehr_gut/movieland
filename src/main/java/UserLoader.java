import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Yurij_000 on 6/7/2016.
 */
public class UserLoader {
    private static final String USERS = "resources/user.txt";

    public static void main(String[] args) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(USERS))) {
            String message;
            String sql = "'";
            String sql_template = "insert into users (name, mail, password) values (";
            Byte steps = 0;
            while ((message = bufferedReader.readLine()) != null) {
                //System.out.println("insert into genres (name) values  ('" + message + "');");
                sql = sql+(sql.length()==1?"":",'") + message+"'";
                steps++;
                if (steps == 3) {
                    System.out.println(sql_template+sql+");");
                }
                if (steps == 4) {
                    sql = "'";
                    steps = 0;
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
